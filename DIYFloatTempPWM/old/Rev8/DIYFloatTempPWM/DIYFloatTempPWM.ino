/*  Reef POM DIY Float Temp PWM v8   (AKA The Ryan)
 *   
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *

Arduino Pinout: 
Pin   Function
--------------
2     Float2Pin
3     PWM1Pin
4     Float1Pin
5     PWM2Pin
6     PWM3Pin
7     Float3Pin
8     Float4Pin
9     PWM4Pin
10    PWM5Pin
11    PWM6Pin
12    Float6Pin
13    Float5Pin
14    DIYTemp3Pin
15    DIYTemp8Pin
16    DIYTemp7Pin
17    DIYTemp6Pin
18    DIYTemp5Pin
19    DIYTemp4Pin
20    DIYTemp1Pin
21    DIYTemp2Pin

Ryan Hub Element List:
Element, Function, Pin#
------------------------
1     , Float#1   , 4
2     , Float#2   , 2
3     , Float#3   , 7
4     , Float#4   , 8
5     , Float#5   , 13
6     , Float#6   , 12
7     , Temp#1    , 20
8     , Temp#2    , 21
9     , Temp#3    , 14
10    , Temp#4    , 19
11    , Temp#5    , 18
12    , Temp#6    , 17
13    , Temp#7    , 16
14    , Temp#8    , 15
15    , PWM#1     , 3
16    , PWM#2     , 5
17    , PWM#3     , 6
18    , PWM#4     , 9
19    , PWM#5     , 10
20    , PWM#6     , 11
*/

#include <EEPROMex.h>  //For Calibration Data and ID

#include <Ospom.h>  //include the ospom library
#include <EEPROMex.h>  //This must be included for Ospom to work
//#include <CapacitiveSensor.h>
Ospom ospom;  // instantiate ospom, an instance of Ospom Library  Don't put () if it isn't getting a variable

//**If doing Triac Dimming, set phase pin in Ospom.cpp at line 682**  ToDo: make this setable here & internet

void setup() {  // put your setup code here, to run once:
  ospom.Setup();  //ospom.Setup initilizes ospom 
}

void loop() {
  ospom.Run(333);  //Activates the ospom library every time the loop runs **This is required
        //(333 = Read Sensors 3 times a second, 1000 = 1 time a second, )
    /*
    Put any standard arduino code in here that you want to run over and over again.
    OSPOM Functions:
    ospom.define(Pin Number, Pin Type, PinFunction, PinID);  = Define OSPOM Internet Dashboard accessable Sensor or Actuator Elements
               example: ospom.define(7, 's', ospom.anaRead, "stfw0000");
    ospom.read();  = Read OSPOM Sensors or Actuators and recieve the result
    ospom.write();  = Write OSPOM Sensors or Actuators using this function
       **Writing a pin that is part of the OSPOM dashboard without using this function will cause
           the dashboard to be confused
    
    **Dont forget to Write your own OSPOM Configuation Sketch to setup your arduino, 
       or run the OSPOM Generic Configuration sketch at least once before calling any ospom functions
    */
}




