/*Core Power Bar Basic Analog Readout Sketch
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *

/*
Arduino Pinout: 
2 AC Phase Input
11 Traic #1 (new)
3 Triac #2
4 Triac #3
5 0-10v #1
6 0-10v #4
7 Triac #4
8 Bottom LED
9 0-10v #2
10 0-10v #3
ADC6 Thermisor (new)  <-- Measures temp of Power Bar
ADC7 pH Reading
14 Right LED
15 Center LED
16 Left LED
17 Top LED
18 DIY Pin #2 (SDA)
19 DIY Pin #1 (SCL)

  Sketch sends data when sent a serial "!"
*/

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

const int PhasePin = 2;
const int pHPin = A7;
float pHValue = 0;
long pHAvg = 0;
const int TempPin = A6;  //Temperature of Power Bar
int Temp = 0;  //Temperature of Power Bar
const int TankTempPin = 19;  //Temperature of Aquarium (Remote thermistor)
float TankTemp = 0;  //Temperature of Aquarium (Remote thermistor)
long TankTempAvg = 0;

const int BottomLEDPin = 8;
const int LeftLEDPin = 16;
const int RightLEDPin = 14;
const int CenterLEDPin = 15;


void setup() {
  // initialize serial:
  Serial.begin(9600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
  
  //Set analog pins as inputs
  pinMode(PhasePin, INPUT);
  pinMode(TempPin, INPUT);
  pinMode(TankTempPin, INPUT);
  Serial.println("pH, Temp");
  
  pinMode(BottomLEDPin, OUTPUT);
  pinMode(LeftLEDPin, OUTPUT);
  pinMode(RightLEDPin, OUTPUT);
  pinMode(CenterLEDPin, OUTPUT); 
  digitalWrite(BottomLEDPin, LOW); 
  digitalWrite(LeftLEDPin, LOW); 
  digitalWrite(RightLEDPin, LOW); 
  digitalWrite(CenterLEDPin, LOW); 

}

void loop() {
  // print the string when a newline arrives:
  if (stringComplete) {
    Serial.print(pHValue);
    Serial.print(",");
    Serial.println(TankTemp);
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
  
  for (int i=0; i<10; i++) {
    TankTempAvg += analogRead(TankTempPin);
    pHAvg += analogRead(pHPin);
  }
  TankTemp = TankTempAvg / 10;
  pHValue = pHAvg / 10;
  
  TankTempAvg = 0;  //Reset average
  pHAvg = 0;  //Reset average
  
/*  TankTemp = TankTemp - 179.3788;  //Convert to Farenheight 
  TankTemp = TankTemp / 4.4138; //y=4.4138x+179.3788
  
   pHValue = pHValue + 435.3;  //Calibrating pH
   pHValue = pHValue / 123.3;  //y=123.3x-435.3
*/  
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '!') {
      stringComplete = true;
    }
  }
}
