/*  Reef POM Fiber Optic Hub v7
 * 
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *
 *
Arduino Pinout: 
Pin   Function
---------------
2     DIY #5
3     DIY #6
4     Leak Sensor 3.5mm Jack
5     IR-reciever 500ohm to 5v
6     IR-reciever 1kohm to 5v
7     IR-reference 3k to 5v
8     IR-reference 15k to 5v
  9     IR-reference 43k to 5v
10    IR-Send pin
11    DIY #8
12    DIY #7
13    DIY #9
14    IR-Recieve pin
15    3.3v Reference for volt meter
16    Not Used
17    Not Used
18    DIY #3
19    DIY #4
20 (ADC6)  DIY #1
21 (ADC7)  DIY #2

Fiber Optic Hub Element List
Element, Function, Pin#
1  ,  Leak Sense  , 4 
2  ,  Voltage     , 15 
3  ,  SD1 PAR     , 
4  ,  SD1 Temp
5  ,  SD1 Flow
6  ,  SD1 Wet / dry
7  ,  DS1 Voltage of battery
8  ,  SD2 PAR
9  ,  SD2 Temp
10 ,  SD2 Flow
11 ,  SD2 Wet / dry
12 ,  SD2 Voltage of battery
13 ,  SD3 PAR
14 ,  SD3 Temp
15 ,  SD3 Flow
16 ,  SD3 Wet / dry
17 ,  SD3 Voltage of battery
18 ,  DIY#1       , 20 
19 ,  DIY#2       , 21 
20 ,  DIY#3       , 18 
 ,  DIY#4       , 19   //Not enough element spaces for these 6 elements
 ,  DIY#5       , 2 
 ,  DIY#6       , 3  
 ,  DIY#7       , 12 
 ,  DIY#8       , 11
 ,  DIY#9       , 13
  Note:The Fiber Hub has more than 20 elements (OSPOM Max).  It will come
         pre-loaded with this Element setup.  If you want to use the other
         DIY pins and you dont have 2 or 3 sundrops, convert those elements
         from SunDrop elements to DIY elements.
*/

#include <Ospom.h>  //include the ospom library
#include <EEPROMex.h>  //This must be included for Ospom to work
//#include <CapacitiveSensor.h>
Ospom ospom;  // instantiate ospom, an instance of Ospom Library  Don't put () if it isn't getting a variable

//**If doing Triac Dimming, set phase pin in Ospom.cpp at line 682**  ToDo: make this setable here & internet

void setup() {  // put your setup code here, to run once:
  ospom.Setup();  //ospom.Setup initilizes ospom 
}

void loop() {
  ospom.Run(333);  //Activates the ospom library every time the loop runs **This is required
        //Delay in milliseconds between reads (333 = Read Sensors 3 times a second, 1000 = 1 time a second.)
    /*
    Put any standard arduino code in here that you want to run over and over again.
    OSPOM Functions:
    ospom.define(Pin Number, Pin Type, PinFunction, PinID);  = Define OSPOM Internet Dashboard accessable Sensor or Actuator Elements
               example: ospom.define(7, 's', ospom.anaRead, "stfw0000");
    ospom.read();  = Read OSPOM Sensors or Actuators and recieve the result
    ospom.write();  = Write OSPOM Sensors or Actuators using this function
       **Writing a pin that is part of the OSPOM dashboard without using this function will cause
           the dashboard to be confused
    ospom.Set(element Number(int)), value(float));  = Send your sensor reading to an ospom element so it is displayed on the dashboard
    
    **Dont forget to Write your own OSPOM Configuation Sketch to setup your arduino, 
       or run the OSPOM Generic Configuration sketch at least once before calling any ospom functions
    */

  //Read the SunDrops and Leak Sensor in here, then send the data to the ospom elements for web access
  unsigned long Duration;
  
  Duration = pulseIn(4, LOW);  //reads length of a pulse in microseconds
  if (Duration != 0) {
    ospom.Set(1, float(Duration));  //Sets ospom Element #1 (Leak Sensor) to the amount of water measured
  }                                 //Higher = more wet
}

