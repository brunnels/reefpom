
/*This program loads the Fiber Optic Hub v7 with necessary EEPROM data.
 *Run this program first, then the real program named for the PCB
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *
 *
Arduino Pinout: 
Pin   Function
---------------
2     DIY #5
3     DIY #6
4     Leak Sensor 3.5mm Jack
5     IR-reciever 500ohm to 5v
6     IR-reciever 1kohm to 5v
7     IR-reference 3k to 5v
8     IR-reference 15k to 5v
9     IR-reference 43k to 5v
10    IR-Send pin
11    DIY #8
12    DIY #7
13    DIY #9
14    IR-Recieve pin
15    3.3v Reference for volt meter
16    Not Used
17    Not Used
18    DIY #3
19    DIY #4
20 (ADC6)  DIY #1
21 (ADC7)  DIY #2

Fiber Optic Hub Element List
Element, Function, Pin#
1  ,  Leak Sense  , 4 
2  ,  Voltage     , 15 
3  ,  SD1 PAR     , 
4  ,  SD1 Temp
5  ,  SD1 Flow
6  ,  SD1 Wet / dry
7  ,  DS1 Voltage of battery
8  ,  SD2 PAR
9  ,  SD2 Temp
10 ,  SD2 Flow
11 ,  SD2 Wet / dry
12 ,  SD2 Voltage of battery
13 ,  SD3 PAR
14 ,  SD3 Temp
15 ,  SD3 Flow
16 ,  SD3 Wet / dry
17 ,  SD3 Voltage of battery
18 ,  DIY#1       , 20 
19 ,  DIY#2       , 21 
20 ,  DIY#3       , 18 
 ,  DIY#4       , 19   //Not enough element spaces for these 6 elements
 ,  DIY#5       , 2 
 ,  DIY#6       , 3  
 ,  DIY#7       , 12 
 ,  DIY#8       , 11
 ,  DIY#9       , 13
  Note:The Fiber Hub has more than 20 elements (OSPOM Max).  It will come
         pre-loaded with this Element setup.  If you want to use the other
         DIY pins and you dont have 2 or 3 sundrops, convert those elements
         from SunDrop elements to DIY elements.
*/

#include <EEPROMex.h>  //For Calibration Data and ID

void setup() {
    // put your setup code here, to run once:
    //Load stock Sensor Element ID's
  EEPROM.updateBlock<char>(0,"gfh00000", 8);   //ID
      
  EEPROM.update(8, 1);  //Enable Watchdog Timer
  EEPROM.updateBlock<char>(9,"slk00001", 8);   //Element #1 ID
  EEPROM.updateBlock<char>(28,"svo00001", 8);  //Element #2 ID
  EEPROM.updateBlock<char>(47,"spr00001", 8);  //Element #3 ID
  EEPROM.updateBlock<char>(66,"stfw0001", 8);  //Element #4 ID
  EEPROM.updateBlock<char>(85,"sfl00001", 8);  //Element #5 ID
  EEPROM.updateBlock<char>(104,"swt00001", 8);  //Element #6 ID
  EEPROM.updateBlock<char>(123,"svb00001", 8);  //Element #7 ID
  EEPROM.updateBlock<char>(142,"spr00002", 8);  //Element #8 ID
  EEPROM.updateBlock<char>(161,"stfw0002", 8);  //Element #9 ID
  EEPROM.updateBlock<char>(180,"sfl00002", 8);  //Element #10 ID
  EEPROM.updateBlock<char>(199,"swt00002", 8);  //Element #11 ID
  EEPROM.updateBlock<char>(218,"svb00002", 8);  //Element #12 ID
  EEPROM.updateBlock<char>(237,"spr00003", 8);  //Element #13 ID
  EEPROM.updateBlock<char>(256,"stfw0003", 8);  //Element #14 ID
  EEPROM.updateBlock<char>(275,"sfl00003", 8);  //Element #15 ID  
  EEPROM.updateBlock<char>(294,"swt00003", 8);  //Element #16 ID
  EEPROM.updateBlock<char>(313,"svb00003", 8);  //Element #17 ID
  EEPROM.updateBlock<char>(332,"sdi00001", 8);  //Element #18 ID
  EEPROM.updateBlock<char>(351,"sdi00002", 8);  //Element #19 ID
  EEPROM.updateBlock<char>(370,"sdi00003", 8);  //Element #20 ID  //Fill the unused ones with z's

    //write stock Element Types
        //s=sensor, a=actuator, n=nothing, z=Loaded by this program
  EEPROM.updateBlock<char>(17,"s", 1);  //Element #1 Type
  EEPROM.updateBlock<char>(36,"s", 1);  //Element #2 Type
  EEPROM.updateBlock<char>(55,"s", 1);  //Element #3 Type
  EEPROM.updateBlock<char>(74,"s", 1);  //Element #4 Type
  EEPROM.updateBlock<char>(93,"s", 1);  //Element #5 Type
  EEPROM.updateBlock<char>(112,"s", 1);  //Element #6 Type
  EEPROM.updateBlock<char>(131,"s", 1);  //Element #7 Type
  EEPROM.updateBlock<char>(150,"s", 1);  //Element #8 Type
  EEPROM.updateBlock<char>(169,"s", 1);  //Element #9 Type
  EEPROM.updateBlock<char>(188,"s", 1);  //Element #10 Type
  EEPROM.updateBlock<char>(207,"s", 1);  //Element #11 Type
  EEPROM.updateBlock<char>(226,"s", 1);  //Element #12 Type
  EEPROM.updateBlock<char>(245,"s", 1);  //Element #13 Type
  EEPROM.updateBlock<char>(264,"s", 1);  //Element #14 Type
  EEPROM.updateBlock<char>(283,"s", 1);  //Element #15 Type  
  EEPROM.updateBlock<char>(302,"s", 1);  //Element #16 Type
  EEPROM.updateBlock<char>(321,"s", 1);  //Element #17 Type
  EEPROM.updateBlock<char>(340,"z", 1);  //Element #18 Type
  EEPROM.updateBlock<char>(359,"z", 1);  //Element #19 Type
  EEPROM.updateBlock<char>(378,"z", 1);  //Element #20 Type  //Fill the unused ones with z's

    //Write stock Element Functions
       //0=InActive, 1=analogRead, 2=digitalRead,
       //3=analogWrite, 4=digitalWrite, 5=triac, 7=level, 8=custom sensor
       //9=custom actuator, 10=Empty/Not an Element
  EEPROM.updateInt(18, 8);  //Elemenr #1 Function
  EEPROM.updateInt(37, 1);  //Elemenr #2 Function
  EEPROM.updateInt(56, 8);  //Elemenr #3 Function
  EEPROM.updateInt(75, 8);  //Elemenr #4 Function
  EEPROM.updateInt(94, 8);  //Elemenr #5 Function
  EEPROM.updateInt(113, 8);  //Elemenr #6 Function
  EEPROM.updateInt(132, 8);  //Elemenr #7 Function
  EEPROM.updateInt(151, 8);  //Elemenr #8 Function
  EEPROM.updateInt(170, 8);  //Elemenr #9 Function
  EEPROM.updateInt(189, 8);  //Elemenr #10 Function
  EEPROM.updateInt(208, 8);  //Elemenr #11 Function
  EEPROM.updateInt(227, 8);  //Elemenr #12 Function
  EEPROM.updateInt(246, 8);  //Elemenr #13 Function
  EEPROM.updateInt(265, 8);  //Elemenr #14 Function
  EEPROM.updateInt(284, 8);  //Elemenr #15 Function
  EEPROM.updateInt(303, 8);  //Elemenr #16 Function
  EEPROM.updateInt(322, 8);  //Elemenr #17 Function
  EEPROM.updateInt(341, 0);  //Elemenr #18 Function  //DIY Pins start as 0, meaning not currently in use
  EEPROM.updateInt(360, 0);  //Elemenr #19 Function
  EEPROM.updateInt(379, 0);  //Elemenr #20 Function

    //Write stock Calibration data if the element is a sensor 
        //or Fail Safe Values / Extras they are actuators
  EEPROM.updateFloat(20, 1);  //Element #1 Slope / FS Val
  EEPROM.updateFloat(24, 0);  //Element #1 Y-Intercept / Extra
  EEPROM.updateFloat(39, -140.8);  //Element #2 Slope / FS Val      Voltage Y=-140.8x+1408 (if vref is 3.3)
  EEPROM.updateFloat(43, 1408);  //Element #2 Y-Intercept / Extra
  EEPROM.updateFloat(58, 1);  //Element #3 Slope / FS Val
  EEPROM.updateFloat(62, 0);  //Element #3 Y-Intercept / Extra
  EEPROM.updateFloat(77, 1);  //Element #4 Slope / FS Val      Temp y=4.4138x+179.3788
  EEPROM.updateFloat(81, 0);  //Element #4 Y-Intercept / Extra
  EEPROM.updateFloat(96, 1);  //Element #5 Slope / FS Val
  EEPROM.updateFloat(100, 0);  //Element #5 Y-Intercept / Extra
  EEPROM.updateFloat(115, 1);  //Element #6 Slope / FS Val
  EEPROM.updateFloat(119, 0);  //Element #6 Y-Intercept / Extra
  EEPROM.updateFloat(134, 1);  //Element #7 Slope / FS Val
  EEPROM.updateFloat(138, 0);  //Element #7 Y-Intercept / Extra
  EEPROM.updateFloat(153, 1);  //Element #8 Slope / FS Val
  EEPROM.updateFloat(157, 0);  //Element #8 Y-Intercept / Extra
  EEPROM.updateFloat(172, 1);  //Element #9 Slope / FS Val
  EEPROM.updateFloat(176, 0);  //Element #9 Y-Intercept / Extra
  EEPROM.updateFloat(191, 1);  //Element #10 Slope / FS Val
  EEPROM.updateFloat(195, 0);  //Element #10 Y-Intercept / Extra
  EEPROM.updateFloat(210, 1);  //Element #11 Slope / FS Val
  EEPROM.updateFloat(214, 0);  //Element #11 Y-Intercept / Extra
  EEPROM.updateFloat(229, 1);  //Element #12 Slope / FS Val
  EEPROM.updateFloat(233, 0);  //Element #12 Y-Intercept / Extra
  EEPROM.updateFloat(248, 1);  //Element #13 Slope / FS Val
  EEPROM.updateFloat(252, 0);  //Element #13 Y-Intercept / Extra
  EEPROM.updateFloat(267, 1);  //Element #14 Slope / FS Val
  EEPROM.updateFloat(271, 0);  //Element #14 Y-Intercept / Extra
  EEPROM.updateFloat(286, 1);  //Element #15 Slope / FS Val
  EEPROM.updateFloat(290, 0);  //Element #15 Y-Intercept / Extra
  EEPROM.updateFloat(305, 1);  //Element #16 Slope / FS Val
  EEPROM.updateFloat(309, 0);  //Element #16 Y-Intercept / Extra
  EEPROM.updateFloat(324, 1);  //Element #17 Slope / FS Val
  EEPROM.updateFloat(328, 0);  //Element #17 Y-Intercept / Extra
  EEPROM.updateFloat(343, 1);  //Element #18 Slope / FS Val
  EEPROM.updateFloat(347, 0);  //Element #18 Y-Intercept / Extra
  EEPROM.updateFloat(362, 1);  //Element #19 Slope / FS Val
  EEPROM.updateFloat(366, 0);  //Element #19 Y-Intercept / Extra
  EEPROM.updateFloat(381, 1);  //Element #20 Slope / FS Val
  EEPROM.updateFloat(385, 0);  //Element #20 Y-Intercept / Extra

  
  //Element Pins
  EEPROM.updateInt(389, 4);  //Element #1 Pin
  EEPROM.updateInt(391, 15);  //Element #2 Pin
  EEPROM.updateInt(393, 99);  //Element #3 Pin
  EEPROM.updateInt(395, 99);  //Element #4 Pin
  EEPROM.updateInt(397, 99);  //Element #5 Pin
  EEPROM.updateInt(399, 99);  //Element #6 Pin
  EEPROM.updateInt(401, 99);  //Element #7 Pin
  EEPROM.updateInt(403, 99);  //Element #8 Pin
  EEPROM.updateInt(405, 99);  //Element #9 Pin
  EEPROM.updateInt(407, 99);  //Element #10 Pin
  EEPROM.updateInt(409, 99);  //Element #11 Pin
  EEPROM.updateInt(411, 99);  //Element #12 Pin
  EEPROM.updateInt(413, 99);  //Element #13 Pin
  EEPROM.updateInt(415, 99);  //Element #14 Pin
  EEPROM.updateInt(417, 99);  //Element #15 Pin
  EEPROM.updateInt(419, 99);  //Element #16 Pin
  EEPROM.updateInt(421, 99);  //Element #17 Pin
  EEPROM.updateInt(423, 20);  //Element #18 Pin
  EEPROM.updateInt(425, 21);  //Element #19 Pin
  EEPROM.updateInt(427, 18);  //Element #20 Pin  //99 signifies no pin
}

void loop() {
  // put your main code here, to run repeatedly:

}
