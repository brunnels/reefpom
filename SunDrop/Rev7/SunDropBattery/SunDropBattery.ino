/*Sun Drop 7th Order Battery Version IR-Send Only Ai Speak
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *

/*
Sun Drop Rev.7 Pinout
0 -IRrecieve
1 -IRsend
3 -CapSense Fill
4 -CapSense Read
5 -IR Recieve V+ 1k ohm
6 -IR Recieve V+ 500 ohm
7 -Power On for Sensors
8 -IR send LED
9 -Flow (Heated) Power (Thermistor 1)
10 -IR Recieve Reference voltage 3k ohm
11 -IR Recieve Reference voltage 15k ohm
12 -IR Recieve Reference voltage 43k ohm
13 -Flow Heater Pin  *HIGH = ON, LOW = OFF
15 -FLow Reading
16 -PAR Reading
18 -VoltageTest Reading
19 -Temp Reading

Sends Data every 30 seconds.
Flow Reading 1x min
*/

#include <CapacitiveSensor.h>
#include <Narcoleptic.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(0, 1); // RX, TX
long capVal = 0;
long capValAvg = 0;
CapacitiveSensor   levelSensor1 = CapacitiveSensor(3,4); //Definng which pins are the (Fill,Sense) pins.

const int IRPwr1kPin = 5;    //For IR Recieve
const int IRPwr500Pin = 6;  //For IR Recieve
const int IRRef3kPin = 10;  //For IR Recieve
const int IRRef15kPin = 11;  //For IR Recieve
const int IRRef43kPin = 12;  //For IR Recieve

const int VReadPin = 18;
const int FlowPwrPin = 9;    // P-mosfet  (LOW = on)
const int FlowHeatPin = 13;  // N-mosfet  (HIGH = on)
const int FlowReadPin = 15;
const int TempReadPin = 19;
const int PARPin = 16; 
const int PwrOnPin = 7;    // P-mosfet
int Flow = 0;
int Flow1 = 0;
int Flow2 = 0;
int Flow3 = 0;
int Flow4 = 0;
int Flow5 = 0;
int Flow1Avg = 0;
int Flow2Avg = 0;
int Flow3Avg = 0;
int Flow4Avg = 0;
int Flow5Avg = 0;
int Temp;
int TempAvg;
int PAR;
int PARAvg;
int Voltage;
int VoltageAvg;
int input;
boolean FlowSend;
int x = 0;

void setup() {
  mySerial.begin(4800);   //Software Serial deals better with Narcoleptic
  
  pinMode (IRPwr1kPin, OUTPUT);
  pinMode (IRPwr500Pin, OUTPUT);
  pinMode (IRRef3kPin, OUTPUT);
  pinMode (IRRef15kPin, OUTPUT);
  pinMode (IRRef43kPin, OUTPUT);
  
  pinMode (FlowPwrPin, OUTPUT);
  pinMode (FlowReadPin, INPUT);
  pinMode (TempReadPin, INPUT);
  pinMode (VReadPin, INPUT);
  pinMode (PARPin, INPUT);
  pinMode (PwrOnPin, OUTPUT);

 
  levelSensor1.set_CS_Timeout_Millis(4000);  //setting up cap-sense
  levelSensor1.set_CS_AutocaL_Millis(0xFFFFFFFF);
 
  digitalWrite (IRPwr1kPin, LOW);
  digitalWrite (IRPwr500Pin, LOW);
  digitalWrite (IRRef3kPin, LOW);
  digitalWrite (IRRef15kPin, LOW);
  digitalWrite (IRRef43kPin, LOW);
  
  delay(1000);
  analogReference(INTERNAL);  //Use the internal 1.1 volt analog reference
}


// the loop routine runs over and over again forever:
void loop() {
  delay(10);  //give the sensors time to warm up
  x++;  //Count to take Flow Readings every 10 mins.
    //Turn on power to testing circuits
  digitalWrite(PwrOnPin, LOW);  //LOW = ON

//*****Change Averaging to the array type*****

    //Sensor Reading Section 10x then avg
  for (int i=0; i<10; i++) {
    capVal += levelSensor1.capacitiveSensorRaw(100);  //reading cap-sense
    Temp += analogRead(TempReadPin);  //Thermistor 2 Read
    PAR += analogRead(PARPin);  //PAR Section
    Voltage += analogRead(VReadPin);  //Voltage Reading Section
  }
    //Averaging the Sensor Readings
  capValAvg = capVal / 10;
  capVal = 0;
  TempAvg = Temp / 10;
  Temp = 0;
  PARAvg = PAR / 10;
  PAR = 0;
  VoltageAvg = Voltage / 10;
  Voltage = 0;  

  //IR Send Section
  mySerial.println("gsd00001");
  mySerial.println(capValAvg);
  mySerial.println(PARAvg);   
  mySerial.println(TempAvg);
  mySerial.println(VoltageAvg);
  
  if (FlowSend) {
    mySerial.println("*");
    mySerial.println("gsd00001");
    mySerial.println(Flow1Avg);
    mySerial.println(Flow2Avg);
    mySerial.println(Flow3Avg);
    mySerial.println(Flow4Avg);
    mySerial.println(Flow5Avg);
    FlowSend = false;
    Flow = 0;
  }

  if (x > 9) {  //Read Flow once every 10 mins
    x=0;  //Reset the counter
    digitalWrite(FlowHeatPin, HIGH);  //turn on for 2 seconds;
    delay(10);
    Narcoleptic.delay(1940);
    delay(50);
    digitalWrite(FlowHeatPin, LOW);  
    delay(10);
    Narcoleptic.delay(3900);
    delay(50);
    digitalWrite(FlowPwrPin, LOW);  //LOW =ON : Turn on flow thermistor
    for (int y = 0; y < 10; y++) {
      Flow1 += analogRead(FlowReadPin);  //Flow Read 
    }
    delay(10);
    Narcoleptic.delay(900);
    delay(50);
    for (int y1 = 0; y1 < 10; y1++) {
      Flow2 += analogRead(FlowReadPin);  //Flow Read 
    }
    delay(10);
    Narcoleptic.delay(900);
    delay(50);
    for (int y2 = 0; y2 < 10; y2++) {
      Flow3 += analogRead(FlowReadPin);  //Flow Read 
    }
    delay(10);
    Narcoleptic.delay(900);
    delay(50);
    for (int y3 = 0; y3 < 10; y3++) {
      Flow4 += analogRead(FlowReadPin);  //Flow Read 
    }
    delay(10);
    Narcoleptic.delay(900);
    delay(50);
    for (int y4 = 0; y4 < 10; y4++) {
      Flow5 += analogRead(FlowReadPin);  //Flow Read
    }
    
    Flow1Avg = Flow1 / 10;
    Flow1 = 0;
    Flow2Avg = Flow2 / 10;
    Flow2 = 0;
    Flow3Avg = Flow3 / 10;
    Flow3 = 0;
    Flow4Avg = Flow4 / 10;
    Flow4 = 0;
    Flow5Avg = Flow5 / 10;
    Flow5 = 0;
    FlowSend = true;  //Let the Send Section know to send Flow Readings
  }

  //Put the SunDrop to sleep for 30 seconds
  digitalWrite(FlowPwrPin, HIGH);  //HIGH = OFF
  digitalWrite(PwrOnPin, HIGH);  //HIGH = OFF
  digitalWrite(FlowHeatPin, LOW);  //Make sure Flow Heat is off
  delay(10);
  Narcoleptic.delay(3000);  //change these to 15000 when finalized
  Narcoleptic.delay(3000);
}
