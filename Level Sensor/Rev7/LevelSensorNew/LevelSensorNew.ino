/* Level Sensor Rev7&8 Ai Speak
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *

Arduino Pinout: 
2 = capsense fill 1
3 = capsense read 1
4 = capsense read 2
5 = capsense read 6
6 = capsense read 7
7 = capsense read 8
8 = capsense read 9
9 = capsense read 10
10 = capsense read 5
11 = capsense read 4
12 = capsense read 11
13 = capsense read 3
14 = Flow senser power
15 = Ambient Light Sensor
16 = Temp read
17 = Flow read
21 = 3.3v reference
*/

#include <Ospom.h>  //include the ospom library
#include <EEPROMex.h>  //This must be included for Ospom to work
Ospom ospom;  // instantiate ospom, an instance of Ospom Library  Don't put () if it isn't getting a variable

//**If doing Triac Dimming, set phase pin in Ospom.cpp at line 682**  ToDo: make this setable here & internet

#include <CapacitiveSensor.h>    //Capacitive Sensing Library
#include <Wire.h>       //I2C Library
#include "TLC59108.h"   //LED Controller Library

#define HW_RESET_PIN 2   //Stuff for I2C LED drivers
#define I2C_ADDR TLC59108::I2C_ADDR::BASE
#define I2C_ADDR2 TLC59108::I2C_ADDR2::BASE2  //2nd led driver
TLC59108 leds(I2C_ADDR);
TLC59108 leds2(I2C_ADDR2);  //2nd led driver



byte LEDHEXbyte;
unsigned long TimeNow = 0;
int levelval = 0;


 //Definng which pins are the (Fill,Sense) pins.
 /*
CapacitiveSensor   LevelSensor11 = CapacitiveSensor(2,12);
CapacitiveSensor   LevelSensor10 = CapacitiveSensor(2,9);
CapacitiveSensor   LevelSensor9 = CapacitiveSensor(2,8);
CapacitiveSensor   LevelSensor8 = CapacitiveSensor(2,7);
CapacitiveSensor   LevelSensor7 = CapacitiveSensor(2,6);
CapacitiveSensor   LevelSensor6 = CapacitiveSensor(2,5);
CapacitiveSensor   LevelSensor5 = CapacitiveSensor(2,10);
*/




 


void setup() {
  // put your setup code here, to run once:
  ospom.Setup();  //ospom.Setup initilizes ospom 

   // turn off callibration
   /*
  LevelSensor11.set_CS_Timeout_Millis(4000);
  LevelSensor11.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor10.set_CS_Timeout_Millis(4000);
  LevelSensor10.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor9.set_CS_Timeout_Millis(4000);
  LevelSensor9.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor8.set_CS_Timeout_Millis(4000);
  LevelSensor8.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor7.set_CS_Timeout_Millis(4000);
  LevelSensor7.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor6.set_CS_Timeout_Millis(4000);
  LevelSensor6.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor5.set_CS_Timeout_Millis(4000);
  LevelSensor5.set_CS_AutocaL_Millis(0xFFFFFFFF);
  */


  //Setup I2C for LED's
  Wire.begin();
  leds.init(HW_RESET_PIN);  //operates 1st led driver
  leds.setLedOutputMode(TLC59108::LED_MODE::PWM_IND);
  leds2.init(HW_RESET_PIN);  //operates 2nd led driver
  leds2.setLedOutputMode(TLC59108::LED_MODE::PWM_IND);

  
  for(byte i = 0; i < 0xff; i++) {  //dim on the leds
    leds.setAllBrightness(i);
    leds2.setAllBrightness(i);
    delay(7);
  }
  for(byte i = 0xff; i > 0; i--) {  //dim off the leds to 1%
    leds.setAllBrightness(i);
    leds2.setAllBrightness(i);
    delay(4);
  }

  
}

void loop() {
 ospom.Run(333);  //Activates the ospom library every time the loop runs **This is required
        //(333 = Read Sensors 3 times a second, 1000 = 1 time a second, )
    /*
    Put any standard arduino code in here that you want to run over and over again.
    OSPOM Functions:
    ospom.define(Pin Number, Pin Type, PinFunction, PinID);  = Define OSPOM Internet Dashboard accessable Sensor or Actuator Elements
               example: ospom.define(7, 's', ospom.anaRead, "stfw0000");
    ospom.read();  = Read OSPOM Sensors or Actuators and recieve the result
    ospom.write();  = Write OSPOM Sensors or Actuators using this function
       **Writing a pin that is part of the OSPOM dashboard without using this function will cause
           the dashboard to be confused
    ospom.WebSet(); = Set an OSPOM Element to a value for forwarding to the internet
    
    **Dont forget to Write your own OSPOM Configuation Sketch to setup your arduino, 
       or run the OSPOM Generic Configuration sketch at least once before calling any ospom functions
    */

/*
case 6:  //Run the Level Sensing Function with printout
          getLevel(true);
          //Clear the string:
          InputString = "";
          StringComplete = false;
          Comand = 0;
          Comand1 = 0;
          ActuatorValString = "";
          ActuatorValInt = 0;
          break;   

 case 18:
        //Write Level Sensor Mins to EEPROM
        //LEVEL SENSOR MUST BE OUT OF WATER
        EEPROM.updateInt(178, CapVal1);  //Save value to EEPROM
        EEPROM.updateInt(174, CapVal2);  //Save value to EEPROM
        EEPROM.updateInt(170, CapVal3);  //Save value to EEPROM
        EEPROM.updateInt(166, CapVal4);  //Save value to EEPROM
        EEPROM.updateInt(162, CapVal5);  //Save value to EEPROM
        EEPROM.updateInt(158, CapVal6);  //Save value to EEPROM
        EEPROM.updateInt(154, CapVal7);  //Save value to EEPROM
        EEPROM.updateInt(150, CapVal8);  //Save value to EEPROM
        EEPROM.updateInt(146, CapVal9);  //Save value to EEPROM
        EEPROM.updateInt(142, CapVal10);  //Save value to EEPROM
        EEPROM.updateInt(138, CapVal11);  //Save value to EEPROM
        
        cap1min = EEPROM.readInt(178);  //Save value to EEPROM
        cap2min = EEPROM.readInt(174);  //Save value to EEPROM
        cap3min = EEPROM.readInt(170);  //Save value to EEPROM
        cap4min = EEPROM.readInt(166);  //Save value to EEPROM
        cap5min = EEPROM.readInt(162);  //Save value to EEPROM
        cap6min = EEPROM.readInt(158);  //Save value to EEPROM
        cap7min = EEPROM.readInt(154);  //Save value to EEPROM
        cap8min = EEPROM.readInt(150);  //Save value to EEPROM
        cap9min = EEPROM.readInt(146);  //Save value to EEPROM
        cap10min = EEPROM.readInt(142);  //Save value to EEPROM
        cap11min = EEPROM.readInt(138);  //Save value to EEPROM
*/
    TimeNow = millis();  //Check the time
    delay(2000);
    getLevel(true);
}

//This Measures the Water level
void getLevel(boolean printout) {
  //Variables for testing
int LastWetPad = 0;
int PadChange = 0;
boolean SkipOne = false;



  
  CapacitiveSensor   LevelSensor4 = CapacitiveSensor(2,11);
  CapacitiveSensor   LevelSensor3 = CapacitiveSensor(2,13);
  CapacitiveSensor   LevelSensor2 = CapacitiveSensor(2,4);
  CapacitiveSensor   LevelSensor1 = CapacitiveSensor(2,3);

  LevelSensor4.set_CS_Timeout_Millis(4000);
  LevelSensor4.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor3.set_CS_Timeout_Millis(4000);
  LevelSensor3.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor2.set_CS_Timeout_Millis(4000);
  LevelSensor2.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor1.set_CS_Timeout_Millis(4000);
  LevelSensor1.set_CS_AutocaL_Millis(0xFFFFFFFF);
  
  const int CapsensefillPin1 = 2;
  const int CapsenseReadPin1 = 3;
  const int CapsenseReadPin2 = 4;
  const int CapsenseReadPin6 = 5;
  const int CapsenseReadPin7 = 6;
  const int CapsenseReadPin8 = 7;
  const int CapsenseReadPin9 = 8;
  const int CapsenseReadPin10 = 9;
  const int CapsenseReadPin5 = 10;
  const int CapsenseReadPin4 = 11;
  const int CapsenseReadPin11 = 12;
  const int CapsenseReadPin3 = 13;
  
  //Capacitive sensor
/*
  int CapVal11;
  int CapVal10;
  int CapVal9;
  int CapVal8;
  int CapVal7;
  int CapVal6;
  int CapVal5;
  */
  int CapVal4;
  int CapVal3;
  int CapVal2;
  int CapVal1;

    //level measuring variables
 /* int cap11max = 1150;
  int cap11min = 0;
  int cap10max = 1071;
  int cap10min = 0;
  int cap9max = 1097;
  int cap9min = 0;
  int cap8max = 1391;
  int cap8min = 0;
  int cap7max = 1374;
  int cap7min = 0;
  int cap6max = 1210;
  int cap6min = 0;
  int cap5max = 1192;
  int cap5min = 0;
  */
  int cap4max = 1279;
  int cap4min = 0;
  int cap3max = 1401;
  int cap3min = 0;
  int cap2max = 1638;
  int cap2min = 0;
  int cap1max = 1963;
  int cap1min = 0;

  cap1min = EEPROM.readInt(178);  //Save value to EEPROM
  cap2min = EEPROM.readInt(174);  //Save value to EEPROM
  cap3min = EEPROM.readInt(170);  //Save value to EEPROM
  cap4min = EEPROM.readInt(166);  //Save value to EEPROM
 /*
  cap5min = EEPROM.readInt(162);  //Save value to EEPROM
  cap6min = EEPROM.readInt(158);  //Save value to EEPROM
  cap7min = EEPROM.readInt(154);  //Save value to EEPROM
  cap8min = EEPROM.readInt(150);  //Save value to EEPROM
  cap9min = EEPROM.readInt(146);  //Save value to EEPROM
  cap10min = EEPROM.readInt(142);  //Save value to EEPROM
  cap11min = EEPROM.readInt(138);  //Save value to EEPROM
*/
  CapVal1 = LevelSensor1.capacitiveSensorRaw(4);
  CapVal2 = LevelSensor2.capacitiveSensorRaw(4);
  CapVal3 = LevelSensor3.capacitiveSensorRaw(4);
  CapVal4 = LevelSensor4.capacitiveSensorRaw(4);  
  
  if (TimeNow > 5000) {
    if (CapVal1 < cap1min) {  //if its less than the minimum, make this the new minimum
      cap1min = CapVal1;     
    }
    if (CapVal2 < cap2min) {
      cap2min = CapVal2;
    }
    if (CapVal3 < cap3min) {
      cap3min = CapVal3;
    }
    if (CapVal4 < cap4min) {
      cap4min = CapVal4;
    }
/*    if (CapVal5 < cap5min) {
      cap5min = CapVal5;
    }
    if (CapVal6 < cap6min) {
      cap6min = CapVal6;
    }
    if (CapVal7 < cap7min) {
      cap7min = CapVal7;
    }
    if (CapVal8 < cap8min) {
      cap8min = CapVal8;
    }
    if (CapVal9 < cap9min) {
      cap9min = CapVal9;
    }
    if (CapVal10 < cap10min) {
      cap10min = CapVal10;
    }
    if (CapVal11 < cap11min) {
      cap11min = CapVal11;
    }
  */  
   
    if (CapVal1 > cap1max) {  //if its more than the max, make this the new max
      cap1max = CapVal1;
    }
    if (CapVal2 > cap2max) {
      cap2max = CapVal2;
    }
    if (CapVal3 > cap3max) {
      cap3max = CapVal3;
    }
    if (CapVal4 > cap4max) {
      cap4max = CapVal4;
    }
 /*   if (CapVal5 > cap5max) {
      cap5max = CapVal5;
    }
    if (CapVal6 > cap6max) {
      cap6max = CapVal6;
    }
    if (CapVal7 > cap7max) {
      cap7max = CapVal7;
    }
    if (CapVal8 > cap8max) {
      cap8max = CapVal8;
    }
    if (CapVal9 > cap9max) {
      cap9max = CapVal9;
    }
    if (CapVal10 > cap10max) {
      cap10max = CapVal10;
    }
    if (CapVal11 > cap11max) {
      cap11max = CapVal11;
    }
*/    
  }
  
  boolean Pad1;
  boolean Pad2;
  boolean Pad3;
  boolean Pad4;
  boolean Pad5;
  boolean Pad6;
  boolean Pad7;
  boolean Pad8;
  boolean Pad9;
  boolean Pad10;
  boolean Pad11;
  
  //Check if the pads are wet or dry
/*  if (CapVal11 > cap11min + ((cap11max - cap11min) / 2))  {
    Pad11 = true;  
  }
    else {
      Pad11 = false;
    }
  if (CapVal10 > cap10min + ((cap10max - cap10min) / 2))  {
    Pad10 = true;
  }
    else {
      Pad10 = false;
    }
  if (CapVal9 > cap9min + ((cap9max - cap9min) / 2)) {
    Pad9 = true;
  }
    else {
      Pad9 = false;
    }
  if (CapVal8 > cap8min + ((cap8max - cap8min) / 2)) {
    Pad8 = true;
  }
    else {
      Pad8 = false;
    }
  if (CapVal7 > cap7min + ((cap7max - cap7min) / 2)) {
    Pad7 = true;
  }
    else {
      Pad7 = false;
    }
  if (CapVal6 > cap6min + ((cap6max - cap6min) / 2)) {  
  Pad6 = true;
  }
    else {
      Pad6 = false;
    }  
  if (CapVal5 > cap5min + ((cap5max - cap5min) / 2)) {  
   Pad5 = true;
  }
    else {
      Pad5 = false;
    } 
 */   
  if (CapVal4 > cap4min + ((cap4max - cap4min) / 2)) {
    Pad4 = true;
  }
    else {
      Pad4 = false;
    }
  if (CapVal3 > cap3min + ((cap3max - cap3min) / 2)) { 
    Pad3 = true;
  }
    else {
      Pad3 = false;
    }
  if (CapVal2 > cap2min + ((cap2max - cap2min) / 2)) {
    Pad2 = true;
  }
    else {
      Pad2 = false;
    }
  if (CapVal1 > cap1min + ((cap1max - cap1min) / 2)) {
    Pad1 = true;
  }
    else {
      Pad1 = false;
    }
      
  //Decide What the level is
  if (Pad11) {
    if ((LastWetPad == 11) || (PadChange > 2)){  //make it not flip flop around
      levelval = 110;
      LastWetPad = 11;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad10) {
     if ((LastWetPad == 10) || (PadChange > 2)){
      levelval = 100;
      LastWetPad = 10;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  }
  else if (Pad9) {
     if ((LastWetPad == 9) || (PadChange > 2)){
      levelval = 90;
      LastWetPad = 9;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  }
  else if (Pad8) {
     if ((LastWetPad == 8) || (PadChange > 2)){
      levelval = 80;
      LastWetPad = 8;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  }
  else if (Pad7) {
     if ((LastWetPad == 7) || (PadChange > 2)){
      levelval = 70;
      LastWetPad = 7;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad6) {
     if ((LastWetPad == 6) || (PadChange > 2)){
      levelval = 60;
      LastWetPad = 6;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad5) {
     if ((LastWetPad == 5) || (PadChange > 2)){
      levelval = 50;
      LastWetPad = 5;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad4) {
     if ((LastWetPad == 4) || (PadChange > 2)){
      levelval = 40;
      LastWetPad = 4;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad3) {
     if ((LastWetPad == 3) || (PadChange > 2)){
      levelval = 30;
      LastWetPad = 3;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad2) {
     if ((LastWetPad == 2) || (PadChange > 2)){
      levelval = 20;
      LastWetPad = 2;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad1) {
     if ((LastWetPad == 1) || (PadChange > 2)){
      levelval = 10;
      LastWetPad = 1;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else {
     if ((LastWetPad == 0) || (PadChange > 2)){
      levelval = 0;
      LastWetPad = 0;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  }

  if(printout) {
 //   Serial.print(ID);  //Group ID
    Serial.print("/");
    Serial.print("levelval:");
    Serial.println(levelval);
 /*   Serial.print(cap11max);
    Serial.print(",");
    Serial.print(CapVal11);
    Serial.print(",");
    Serial.println(cap11min);
    Serial.print(cap10max);
    Serial.print(",");
    Serial.print(CapVal10);
    Serial.print(",");
    Serial.println(cap10min);
   
    Serial.print(cap9max);
    Serial.print(",");
    Serial.print(CapVal9);
    Serial.print(",");
    Serial.println(cap9min);
    
    Serial.print(cap8max);
    Serial.print(",");
    Serial.print(CapVal8);
    Serial.print(",");
    Serial.println(cap8min);
   
    Serial.print(cap7max);
    Serial.print(",");
    Serial.print(CapVal7);
    Serial.print(",");
    Serial.println(cap7min);
    
    Serial.print(cap6max);
    Serial.print(",");
    Serial.print(CapVal6);
    Serial.print(",");
    Serial.println(cap6min);
    
    Serial.print(cap5max);
    Serial.print(",");
    Serial.print(CapVal5);
    Serial.print(",");
    Serial.println(cap5min);
 */   
    Serial.print(cap4max);
    Serial.print(",");
    Serial.print(CapVal4);
    Serial.print(",");
    Serial.println(cap4min);
    
    Serial.print(cap3max);
    Serial.print(",");
    Serial.print(CapVal3);
    Serial.print(",");
    Serial.println(cap3min);
    
    Serial.print(cap2max);
    Serial.print(",");
    Serial.print(CapVal2);
    Serial.print(",");
    Serial.println(cap2min);
    
    Serial.print(cap1max);
    Serial.print(",");
    Serial.print(CapVal1);
    Serial.print(",");
    Serial.println(cap1min);
    
    Serial.println(" ");
  }  

  levLED();
} 


//This sets the LED's to show the water level
void levLED(void) {  
  byte off = 0;
  if (levelval == 110) {
    //Turn LED 1-16 on.
    leds.setAllBrightness(LEDHEXbyte);
    leds2.setAllBrightness(LEDHEXbyte);
//    Serial.println("110");
  }
  else if (levelval == 100){
    //Turn LED 1-15 on.  rest off
    leds2.setBrightness(2, LEDHEXbyte);
    leds2.setBrightness(1, LEDHEXbyte);
    leds2.setBrightness(0, LEDHEXbyte);
    leds2.setBrightness(7, LEDHEXbyte);
    leds2.setBrightness(6, LEDHEXbyte);
    leds2.setBrightness(5, LEDHEXbyte);
    leds2.setBrightness(4, LEDHEXbyte);
    leds.setAllBrightness(LEDHEXbyte);
//    Serial.println("100");
  }
  else if (levelval == 90){
    //Turn LED 1-14 on.  rest off
    leds2.setBrightness(1, LEDHEXbyte);
    leds2.setBrightness(0, LEDHEXbyte);
    leds2.setBrightness(7, LEDHEXbyte);
    leds2.setBrightness(6, LEDHEXbyte);
    leds2.setBrightness(5, LEDHEXbyte);
    leds2.setBrightness(4, LEDHEXbyte);
    //Turn the rest off
    leds2.setBrightness(3, off);
    leds2.setBrightness(2, off);
    leds.setAllBrightness(LEDHEXbyte);
//    Serial.println("90");
  }
  else if (levelval == 80){
    //Turn LED 1-12 on.  rest off
    leds2.setBrightness(7, LEDHEXbyte);
    leds2.setBrightness(6, LEDHEXbyte);
    leds2.setBrightness(5, LEDHEXbyte);
    leds2.setBrightness(4, LEDHEXbyte);
    //Turn the rest off
    leds2.setBrightness(3, off);
    leds2.setBrightness(2, off);
    leds2.setBrightness(1, off);
    leds2.setBrightness(0, off);
    leds.setAllBrightness(LEDHEXbyte);
//    Serial.println("80");
  }
  else if (levelval == 70){
    //Turn LED 1-11 on.  rest off
    leds2.setBrightness(6, LEDHEXbyte);
    leds2.setBrightness(5, LEDHEXbyte);
    leds2.setBrightness(4, LEDHEXbyte); 
    //Turn the rest off
    leds2.setBrightness(3, off);
    leds2.setBrightness(2, off);
    leds2.setBrightness(1, off);
    leds2.setBrightness(0, off);
    leds2.setBrightness(7, off);
    leds.setAllBrightness(LEDHEXbyte); 
//    Serial.println("70");
  }
  else if (levelval == 60){
    //Turn LED 1-9 on.  rest off
    leds2.setBrightness(4, LEDHEXbyte);
    //Turn the rest off
    leds2.setBrightness(3, off);
    leds2.setBrightness(2, off);
    leds2.setBrightness(1, off);
    leds2.setBrightness(0, off);
    leds2.setBrightness(7, off);
    leds2.setBrightness(6, off);
    leds2.setBrightness(5, off);
    leds.setAllBrightness(LEDHEXbyte);
//    Serial.println("60");
  }
  else if (levelval == 50) {
    //Turn LED 1-7 on.  rest off
    leds.setBrightness(5, LEDHEXbyte);
    leds.setBrightness(6, LEDHEXbyte);
    leds.setBrightness(7, LEDHEXbyte);
    leds.setBrightness(0, LEDHEXbyte);
    leds.setBrightness(1, LEDHEXbyte);
    leds.setBrightness(2, LEDHEXbyte);
    leds.setBrightness(3, LEDHEXbyte);
    //Turn the rest off
    leds.setBrightness(4, off);
    leds2.setAllBrightness(off);
//    Serial.println("50");
  }
  else if (levelval == 40){  
    //Turn LED 1-6 on.  rest off
    leds.setBrightness(6, LEDHEXbyte);
    leds.setBrightness(7, LEDHEXbyte);
    leds.setBrightness(0, LEDHEXbyte);
    leds.setBrightness(1, LEDHEXbyte);
    leds.setBrightness(2, LEDHEXbyte);
    leds.setBrightness(3, LEDHEXbyte);
    //Turn the rest off
    leds.setBrightness(4, off);
    leds.setBrightness(5, off);
    leds2.setAllBrightness(off);
//    Serial.println("40");
  }
  else if (levelval == 30) {
    //Turn LED 1-4 on.
    leds.setBrightness(0, LEDHEXbyte);
    leds.setBrightness(1, LEDHEXbyte);
    leds.setBrightness(2, LEDHEXbyte);
    leds.setBrightness(3, LEDHEXbyte);
    //Turn the rest off
    leds.setBrightness(4, off);
    leds.setBrightness(5, off);
    leds.setBrightness(6, off);
    leds.setBrightness(7, off);
    leds2.setAllBrightness(off);
//    Serial.println("30");
  }
  else if (levelval == 20){
    //Turn LED 1-3 on
    leds.setBrightness(1, LEDHEXbyte);
    leds.setBrightness(2, LEDHEXbyte);
    leds.setBrightness(3, LEDHEXbyte);
    //Turn the rest off
    leds.setBrightness(4, off);
    leds.setBrightness(5, off);
    leds.setBrightness(6, off);
    leds.setBrightness(7, off);
    leds.setBrightness(0, off);
    leds2.setAllBrightness(off);
//    Serial.println("20");
  }
  else if (levelval == 10) {
    //Turn Bottom LED On #1
    leds.setBrightness(3, LEDHEXbyte);
    //Turn the rest off
    leds.setBrightness(4, off);
    leds.setBrightness(5, off);
    leds.setBrightness(6, off);
    leds.setBrightness(7, off);
    leds.setBrightness(0, off);
    leds.setBrightness(1, off);
    leds.setBrightness(2, off);
    leds2.setAllBrightness(off);
//    Serial.println("10");
  }
  else {
    //Turn all LED's OFF
    leds2.setAllBrightness(off);
    leds.setAllBrightness(off);
//    Serial.println("0");
  } 
}
