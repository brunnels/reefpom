/* Reef POM DIY Float Temp PWM  v9 (AKA The Ryan) Testing for 2 temp probes
 * 
 * This program loads the Ryan Hub with necessary EEPROM data.
 * Run this program first, then the real program named for the PCB
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close, Gary Tomko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *
 *
Arduino Pinout: 
Pin   Function
--------------
2     Float2Pin
11     PWM1Pin
4     Float1Pin
10     PWM2Pin
9     PWM3Pin
7     Float3Pin
8     Float4Pin
6     PWM4Pin
5     PWM5Pin
3     PWM6Pin 
12    Float6Pin
13    Float5Pin
14    DIYTemp3Pin
15    DIYTemp8Pin
16    DIYTemp7Pin
17    DIYTemp6Pin
18    DIYTemp5Pin
19    DIYTemp4Pin
20    DIYTemp1Pin
21    DIYTemp2Pin

Ryan Hub Element List:
Element, Function, Pin#
------------------------
1     , Float#1   , 4
2     , Float#2   , 2
3     , Float#3   , 7
4     , Float#4   , 8
5     , Temp#3    , 14  in use
6     , Temp#4    , 19
7     , Temp#5    , 18
8     , Temp#6    , 17  in use
9     , Temp#7    , 16
10    , PWM#1     , 11
11    , PWM#2     , 10
12    , PWM#3     , 9
13    , PWM#4     , 6
14    , PWM#5     , 5
15    , PWM#6     , 3

Chip doesn't have enough ram for  + the 1wire library
5     , Float#5   , 13
6     , Float#6   , 12
12    , Temp#6    , 17
13    , Temp#7    , 16
14    , Temp#8    , 15
*/

#include <EEPROMex.h>  //For Calibration Data and ID

void setup() {
    // put your setup code here, to run once:
    //Load stock Sensor Element ID's
  EEPROM.updateBlock<char>(0,"gry00000", 8);   //ID
      
  EEPROM.update(8, 1);  //Enable Watchdog Timer
  EEPROM.updateBlock<char>(9,"sfs00001", 8);   //Element #1 ID
  EEPROM.updateBlock<char>(28,"sfs00002", 8);  //Element #2 ID
  EEPROM.updateBlock<char>(47,"sfs00003", 8);  //Element #3 ID
  EEPROM.updateBlock<char>(66,"sfs00004", 8);  //Element #4 ID
  EEPROM.updateBlock<char>(85,"stfw0003", 8);  //Element #5 ID
  EEPROM.updateBlock<char>(104,"stfw0004", 8);  //Element #6 ID
  EEPROM.updateBlock<char>(123,"stfw0005", 8);  //Element #7 ID
  EEPROM.updateBlock<char>(142,"stfw0006", 8);  //Element #8 ID
  EEPROM.updateBlock<char>(161,"stfw0007", 8);  //Element #9 ID
  EEPROM.updateBlock<char>(180,"apw00001", 8);  //Element #10 ID
  EEPROM.updateBlock<char>(199,"apw00002", 8);  //Element #11 ID
  EEPROM.updateBlock<char>(218,"apw00003", 8);  //Element #12 ID
  EEPROM.updateBlock<char>(237,"apw00004", 8);  //Element #13 ID
  EEPROM.updateBlock<char>(256,"apw00005", 8);  //Element #14 ID
  EEPROM.updateBlock<char>(275,"apw00006", 8);  //Element #15 ID  //Fill the unused ones with z's
  EEPROM.updateBlock<char>(294,"zzzzzzzz", 8);  //Element #16 ID
  EEPROM.updateBlock<char>(313,"zzzzzzzz", 8);  //Element #17 ID
  EEPROM.updateBlock<char>(332,"zzzzzzzz", 8);  //Element #18 ID
  EEPROM.updateBlock<char>(351,"zzzzzzzz", 8);  //Element #19 ID
  EEPROM.updateBlock<char>(370,"zzzzzzzz", 8);  //Element #20 ID


    //write stock Element Types
        //s=sensor, a=actuator, z=Loaded by this program
  EEPROM.updateBlock<char>(17,"s", 1);  //Element #1 Type
  EEPROM.updateBlock<char>(36,"s", 1);  //Element #2 Type
  EEPROM.updateBlock<char>(55,"s", 1);  //Element #3 Type
  EEPROM.updateBlock<char>(74,"s", 1);  //Element #4 Type
  EEPROM.updateBlock<char>(93,"s", 1);  //Element #5 Type
  EEPROM.updateBlock<char>(112,"s", 1);  //Element #6 Type
  EEPROM.updateBlock<char>(131,"s", 1);  //Element #7 Type
  EEPROM.updateBlock<char>(150,"s", 1);  //Element #8 Type
  EEPROM.updateBlock<char>(169,"s", 1);  //Element #9 Type
  EEPROM.updateBlock<char>(188,"a", 1);  //Element #10 Type
  EEPROM.updateBlock<char>(207,"a", 1);  //Element #11 Type
  EEPROM.updateBlock<char>(226,"a", 1);  //Element #12 Type
  EEPROM.updateBlock<char>(245,"a", 1);  //Element #13 Type
  EEPROM.updateBlock<char>(264,"a", 1);  //Element #14 Type
  EEPROM.updateBlock<char>(283,"a", 1);  //Element #15 Type  //Fill the unused ones with z's
  EEPROM.updateBlock<char>(302,"z", 1);  //Element #16 Type
  EEPROM.updateBlock<char>(321,"z", 1);  //Element #17 Type
  EEPROM.updateBlock<char>(340,"z", 1);  //Element #18 Type
  EEPROM.updateBlock<char>(359,"z", 1);  //Element #19 Type
  EEPROM.updateBlock<char>(378,"z", 1);  //Element #20 Type


    //Write stock Element Functions
       //0=inactive, 1=analogRead, 2=digitalRead,
       //3=analogWrite, 4=digitalWrite, 5=triac, 7=level, 8=custom sensor
       //9=custom actuator, 10=Empty
  EEPROM.updateInt(18, 0);  //Element #1 Function
  EEPROM.updateInt(37, 0);  //Element #2 Function
  EEPROM.updateInt(56, 0);  //Element #3 Function
  EEPROM.updateInt(75, 0);  //Element #4 Function
  EEPROM.updateInt(94, 8);  //Element #5 Function
  EEPROM.updateInt(113, 0);  //Element #6 Function
  EEPROM.updateInt(132, 0);  //Element #7 Function
  EEPROM.updateInt(151, 8);  //Element #8 Function
  EEPROM.updateInt(170, 0);  //Element #9 Function
  EEPROM.updateInt(189, 3);  //Element #10 Function
  EEPROM.updateInt(208, 3);  //Element #11 Function
  EEPROM.updateInt(227, 3);  //Element #12 Function
  EEPROM.updateInt(246, 3);  //Element #13 Function
  EEPROM.updateInt(265, 3);  //Element #14 Function
  EEPROM.updateInt(284, 3);  //Element #15 Function
  EEPROM.updateInt(303, 10);  //Element #16 Function
  EEPROM.updateInt(322, 10);  //Element #17 Function
  EEPROM.updateInt(341, 10);  //Element #18 Function
  EEPROM.updateInt(360, 10);  //Element #19 Function
  EEPROM.updateInt(379, 10);  //Element #20 Function

    //Write stock Calibration data if the element is a sensor 
        //or Fail Safe Values / Extras they are actuators
  EEPROM.updateFloat(20, 1);  //Element #1 Slope / FS Val
  EEPROM.updateFloat(24, 0);  //Element #1 Y-Intercept / Extra
  EEPROM.updateFloat(39, 1);  //Element #2 Slope / FS Val
  EEPROM.updateFloat(43, 0);  //Element #2 Y-Intercept / Extra
  EEPROM.updateFloat(58, 1);  //Element #3 Slope / FS Val
  EEPROM.updateFloat(62, 0);  //Element #3 Y-Intercept / Extra
  EEPROM.updateFloat(77, 1);  //Element #4 Slope / FS Val
  EEPROM.updateFloat(81, 0);  //Element #4 Y-Intercept / Extra
  EEPROM.updateFloat(96, 1);  //Element #5 Slope / FS Val
  EEPROM.updateFloat(100, 0);  //Element #5 Y-Intercept / Extra
  EEPROM.updateFloat(115, 1);  //Element #6 Slope / FS Val
  EEPROM.updateFloat(119, 0);  //Element #6 Y-Intercept / Extra
  EEPROM.updateFloat(134, 1);  //Element #7 Slope / FS Val        // y=4.4138x+179.3788
  EEPROM.updateFloat(138, 0);  //Element #7 Y-Intercept / Extra
  EEPROM.updateFloat(153, 1);  //Element #8 Slope / FS Val
  EEPROM.updateFloat(157, 0);  //Element #8 Y-Intercept / Extra
  EEPROM.updateFloat(172, 1);  //Element #9 Slope / FS Val
  EEPROM.updateFloat(176, 0);  //Element #9 Y-Intercept / Extra
  EEPROM.updateFloat(191, 1);  //Element #10 Slope / FS Val
  EEPROM.updateFloat(195, 0);  //Element #10 Y-Intercept / Extra
  EEPROM.updateFloat(210, 1);  //Element #11 Slope / FS Val
  EEPROM.updateFloat(214, 0);  //Element #11 Y-Intercept / Extra
  EEPROM.updateFloat(229, 1);  //Element #12 Slope / FS Val
  EEPROM.updateFloat(233, 0);  //Element #12 Y-Intercept / Extra
  EEPROM.updateFloat(248, 1);  //Element #13 Slope / FS Val
  EEPROM.updateFloat(252, 0);  //Element #13 Y-Intercept / Extra
  EEPROM.updateFloat(267, 1);  //Element #14 Slope / FS Val
  EEPROM.updateFloat(271, 0);  //Element #14 Y-Intercept / Extra
  EEPROM.updateFloat(286, 1);  //Element #15 Slope / FS Val
  EEPROM.updateFloat(290, 0);  //Element #15 Y-Intercept / Extra
  EEPROM.updateFloat(305, 1);  //Element #16 Slope / FS Val
  EEPROM.updateFloat(309, 0);  //Element #16 Y-Intercept / Extra
  EEPROM.updateFloat(324, 1);  //Element #17 Slope / FS Val
  EEPROM.updateFloat(328, 0);  //Element #17 Y-Intercept / Extra
  EEPROM.updateFloat(343, 1);  //Element #18 Slope / FS Val
  EEPROM.updateFloat(347, 0);  //Element #18 Y-Intercept / Extra
  EEPROM.updateFloat(362, 1);  //Element #19 Slope / FS Val
  EEPROM.updateFloat(366, 0);  //Element #19 Y-Intercept / Extra
  EEPROM.updateFloat(381, 1);  //Element #20 Slope / FS Val
  EEPROM.updateFloat(385, 0);  //Element #20 Y-Intercept / Extra
  
  //Element Pins
  EEPROM.updateInt(389, 4);  //Element #1 Pin
  EEPROM.updateInt(391, 2);  //Element #2 Pin
  EEPROM.updateInt(393, 7);  //Element #3 Pin
  EEPROM.updateInt(395, 8);  //Element #4 Pin
  EEPROM.updateInt(397, 14);  //Element #5 Pin
  EEPROM.updateInt(399, 19);  //Element #6 Pin
  EEPROM.updateInt(401, 18);  //Element #7 Pin
  EEPROM.updateInt(403, 17);  //Element #8 Pin
  EEPROM.updateInt(405, 16);  //Element #9 Pin
  EEPROM.updateInt(407, 11);  //Element #10 Pin
  EEPROM.updateInt(409, 10);  //Element #11 Pin
  EEPROM.updateInt(411, 9);  //Element #12 Pin
  EEPROM.updateInt(413, 6);  //Element #13 Pin
  EEPROM.updateInt(415, 5);  //Element #14 Pin
  EEPROM.updateInt(417, 3);  //Element #15 Pin
  EEPROM.updateInt(419, 99);  //Element #16 Pin
  EEPROM.updateInt(421, 99);  //Element #17 Pin
  EEPROM.updateInt(423, 99);  //Element #18 Pin
  EEPROM.updateInt(425, 99);  //Element #19 Pin
  EEPROM.updateInt(427, 99);  //Element #20 Pin
}

void loop() {
  // put your main code here, to run repeatedly:

}
