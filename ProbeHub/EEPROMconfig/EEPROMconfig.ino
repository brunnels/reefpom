
/*This program loads the Probe Hub v8 with necessary EEPROM data.
 *Run this program first, then the real program named for the PCB
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *
 *
Probe Hub Arduino Pinout: 
Pin   Function
--------------
2 DIY Pin #5
3 DIY Pin #4
5 LED Brightness(pwm)
11 DIY Pin #2 (mosi)
12 DIY Pin #3 (miso)
13 DIY Pin #1 (sck)
14 pH #2
15 3.3v (test to find board voltage)
16 pH #1
17 Salinity
18 DIY Pin #7  SDA
19 DIY Pin #6 SCL
A6 DIY Pin #9 Analog
A7 PCB Temp

ProbeHub Element List
Element, Function, Pin#
------------------------
1  ,  PCB Temp F  ,  A7 (21)
2  ,  pH 1        ,  16
3  ,  pH 2        ,  14
4  ,  Salinity    ,  17
5  ,  pcb voltage ,  15
6  ,  DIY 1       ,  13
7  ,  DIY 2       ,  11
8  ,  DIY 3       ,  12
9  ,  DIY 4       ,  3
10 ,  DIY 5       ,  2
11 ,  DIY 6       ,  19
12 ,  DIY 7       ,  18
13 ,  DIY 9       ,  20 (A6)
14 ,  LED Val     ,  5

**AC Phase Pin 2 can't be an element, as they arent fast enough
*/

#include <EEPROMex.h>  //For Calibration Data and ID

void setup() {
    // put your setup code here, to run once:
    //Load stock Sensor Element ID's
  EEPROM.updateBlock<char>(0,"gph00000", 8);   //ID
      
  EEPROM.update(8, 1);  //Enable Watchdog Timer
  EEPROM.updateBlock<char>(9,"stfb0003", 8);   //Element #1 ID
  EEPROM.updateBlock<char>(28,"sph00003", 8);  //Element #2 ID
  EEPROM.updateBlock<char>(47,"sph00004", 8);  //Element #3 ID
  EEPROM.updateBlock<char>(66,"ssl00000", 8);  //Element #4 ID
  EEPROM.updateBlock<char>(85,"svo00001", 8);  //Element #5 ID
  EEPROM.updateBlock<char>(104,"sdi00011", 8);  //Element #6 ID
  EEPROM.updateBlock<char>(123,"sdi00012", 8);  //Element #7 ID
  EEPROM.updateBlock<char>(142,"sdi00013", 8);  //Element #8 ID
  EEPROM.updateBlock<char>(161,"sdi00014", 8);  //Element #9 ID
  EEPROM.updateBlock<char>(180,"sdi00015", 8);  //Element #10 ID
  EEPROM.updateBlock<char>(199,"sdi00016", 8);  //Element #11 ID
  EEPROM.updateBlock<char>(218,"sdi00017", 8);  //Element #12 ID
  EEPROM.updateBlock<char>(237,"sdi00018", 8);  //Element #13 ID
  EEPROM.updateBlock<char>(256,"alb00011", 8);  //Element #14 ID
  EEPROM.updateBlock<char>(275,"zzzzzzzz", 8);  //Element #15 ID  //Fill the unused ones with z's
  EEPROM.updateBlock<char>(294,"zzzzzzzz", 8);  //Element #16 ID
  EEPROM.updateBlock<char>(313,"zzzzzzzz", 8);  //Element #17 ID
  EEPROM.updateBlock<char>(332,"zzzzzzzz", 8);  //Element #18 ID
  EEPROM.updateBlock<char>(351,"zzzzzzzz", 8);  //Element #19 ID
  EEPROM.updateBlock<char>(370,"zzzzzzzz", 8);  //Element #20 ID

    //write stock Element Types
        //s=sensor, a=actuator, n=nothing, z=Loaded by this program
  EEPROM.updateBlock<char>(17,"s", 1);  //Element #1 Type
  EEPROM.updateBlock<char>(36,"s", 1);  //Element #2 Type
  EEPROM.updateBlock<char>(55,"s", 1);  //Element #3 Type
  EEPROM.updateBlock<char>(74,"s", 1);  //Element #4 Type
  EEPROM.updateBlock<char>(93,"s", 1);  //Element #5 Type
  EEPROM.updateBlock<char>(112,"s", 1);  //Element #6 Type
  EEPROM.updateBlock<char>(131,"s", 1);  //Element #7 Type
  EEPROM.updateBlock<char>(150,"s", 1);  //Element #8 Type
  EEPROM.updateBlock<char>(169,"s", 1);  //Element #9 Type
  EEPROM.updateBlock<char>(188,"s", 1);  //Element #10 Type
  EEPROM.updateBlock<char>(207,"s", 1);  //Element #11 Type
  EEPROM.updateBlock<char>(226,"s", 1);  //Element #12 Type
  EEPROM.updateBlock<char>(245,"s", 1);  //Element #13 Type
  EEPROM.updateBlock<char>(264,"a", 1);  //Element #14 Type
  EEPROM.updateBlock<char>(283,"z", 1);  //Element #15 Type  //Fill the unused ones with z's
  EEPROM.updateBlock<char>(302,"z", 1);  //Element #16 Type
  EEPROM.updateBlock<char>(321,"z", 1);  //Element #17 Type
  EEPROM.updateBlock<char>(340,"z", 1);  //Element #18 Type
  EEPROM.updateBlock<char>(359,"z", 1);  //Element #19 Type
  EEPROM.updateBlock<char>(378,"z", 1);  //Element #20 Type

    //Write stock Element Functions
       //0=unused, 1=analogRead, 2=digitalRead,
       //3=analogWrite, 4=digitalWrite, 5=triac, 7=level, 10=Empty
  EEPROM.updateInt(18, 1);  //Elemenr #1 Function
  EEPROM.updateInt(37, 1);  //Elemenr #2 Function
  EEPROM.updateInt(56, 1);  //Elemenr #3 Function
  EEPROM.updateInt(75, 1);  //Elemenr #4 Function
  EEPROM.updateInt(94, 1);  //Elemenr #5 Function
  EEPROM.updateInt(113, 0);  //Elemenr #6 Function
  EEPROM.updateInt(132, 0);  //Elemenr #7 Function
  EEPROM.updateInt(151, 0);  //Elemenr #8 Function
  EEPROM.updateInt(170, 0);  //Elemenr #9 Function
  EEPROM.updateInt(189, 0);  //Elemenr #10 Function
  EEPROM.updateInt(208, 0);  //Elemenr #11 Function
  EEPROM.updateInt(227, 0);  //Elemenr #12 Function
  EEPROM.updateInt(246, 0);  //Elemenr #13 Function
  EEPROM.updateInt(265, 3);  //Elemenr #14 Function
  EEPROM.updateInt(284, 10);  //Elemenr #15 Function
  EEPROM.updateInt(303, 10);  //Elemenr #16 Function
  EEPROM.updateInt(322, 10);  //Elemenr #17 Function
  EEPROM.updateInt(341, 10);  //Elemenr #18 Function
  EEPROM.updateInt(360, 10);  //Elemenr #19 Function
  EEPROM.updateInt(379, 10);  //Elemenr #20 Function

    //Write stock Calibration data if the element is a sensor 
        //or Fail Safe Values / Extras they are actuators
  EEPROM.updateFloat(20, 4.4138);  //Element #1 Slope / FS Val    Temp y=4.4138x+179.3788
  EEPROM.updateFloat(24, 179.3788);  //Element #1 Y-Intercept / Extra
  EEPROM.updateFloat(39, 117);  //Element #2 Slope / FS Val         pH y=117x-362
  EEPROM.updateFloat(43, -362);  //Element #2 Y-Intercept / Extra
  EEPROM.updateFloat(58, 117);  //Element #3 Slope / FS Val         pH y=117x-362
  EEPROM.updateFloat(62,-362);  //Element #3 Y-Intercept / Extra
  EEPROM.updateFloat(77, 1);  //Element #4 Slope / FS Val
  EEPROM.updateFloat(81, 0);  //Element #4 Y-Intercept / Extra
  EEPROM.updateFloat(96, -140.8);  //Element #5 Slope / FS Val    Voltage Y=-140.8x+1408 (if vref is 3.3)
  EEPROM.updateFloat(100, 1408);  //Element #5 Y-Intercept / Extra
  EEPROM.updateFloat(115, 1);  //Element #6 Slope / FS Val
  EEPROM.updateFloat(119, 0);  //Element #6 Y-Intercept / Extra
  EEPROM.updateFloat(134, 1);  //Element #7 Slope / FS Val
  EEPROM.updateFloat(138, 0);  //Element #7 Y-Intercept / Extra
  EEPROM.updateFloat(153, 1);  //Element #8 Slope / FS Val
  EEPROM.updateFloat(157, 0);  //Element #8 Y-Intercept / Extra
  EEPROM.updateFloat(172, 1);  //Element #9 Slope / FS Val     
  EEPROM.updateFloat(176, 0);  //Element #9 Y-Intercept / Extra
  EEPROM.updateFloat(191, 1);  //Element #10 Slope / FS Val  
  EEPROM.updateFloat(195, 0);  //Element #10 Y-Intercept / Extra
  EEPROM.updateFloat(210, 1);  //Element #11 Slope / FS Val
  EEPROM.updateFloat(214, 0);  //Element #11 Y-Intercept / Extra
  EEPROM.updateFloat(229, 1);  //Element #12 Slope / FS Val    
  EEPROM.updateFloat(233, 0);  //Element #12 Y-Intercept / Extra
  EEPROM.updateFloat(248, 1);  //Element #13 Slope / FS Val
  EEPROM.updateFloat(252, 0);  //Element #13 Y-Intercept / Extra
  EEPROM.updateFloat(267, 1);  //Element #14 Slope / FS Val
  EEPROM.updateFloat(271, 0);  //Element #14 Y-Intercept / Extra
  EEPROM.updateFloat(286, 1);  //Element #15 Slope / FS Val
  EEPROM.updateFloat(290, 0);  //Element #15 Y-Intercept / Extra
  EEPROM.updateFloat(305, 1);  //Element #16 Slope / FS Val
  EEPROM.updateFloat(309, 0);  //Element #16 Y-Intercept / Extra
  EEPROM.updateFloat(324, 1);  //Element #17 Slope / FS Val
  EEPROM.updateFloat(328, 0);  //Element #17 Y-Intercept / Extra
  EEPROM.updateFloat(343, 1);  //Element #18 Slope / FS Val
  EEPROM.updateFloat(347, 0);  //Element #18 Y-Intercept / Extra
  EEPROM.updateFloat(362, 1);  //Element #19 Slope / FS Val
  EEPROM.updateFloat(366, 0);  //Element #19 Y-Intercept / Extra
  EEPROM.updateFloat(381, 1);  //Element #20 Slope / FS Val
  EEPROM.updateFloat(385, 0);  //Element #20 Y-Intercept / Extra

  EEPROM.updateFloat(473, 0);  //Element #1 C Value
  EEPROM.updateFloat(477, 0);  //Element #2 C Value
  EEPROM.updateFloat(481, 0);  //Element #3 C Value
  EEPROM.updateFloat(485, 0);  //Element #4 C Value
  EEPROM.updateFloat(489, 0);  //Element #5 C Value
  EEPROM.updateFloat(493, 0);  //Element #6 C Value
  EEPROM.updateFloat(497, 0);  //Element #7 C Value
  EEPROM.updateFloat(501, 0);  //Element #8 C Value
  EEPROM.updateFloat(505, 0);  //Element #9 C Value
  EEPROM.updateFloat(509, 0);  //Element #10 C Value
  EEPROM.updateFloat(513, 0);  //Element #11 C Value
  EEPROM.updateFloat(517, 0);  //Element #12 C Value
  EEPROM.updateFloat(521, 0);  //Element #13 C Value
  EEPROM.updateFloat(525, 0);  //Element #14 C Value
  EEPROM.updateFloat(529, 0);  //Element #15 C Value
  EEPROM.updateFloat(533, 0);  //Element #16 C Value


  //Element Pins
  EEPROM.updateInt(389, 21);  //Element #1 Pin
  EEPROM.updateInt(391, 16);  //Element #2 Pin
  EEPROM.updateInt(393, 14);  //Element #3 Pin
  EEPROM.updateInt(395, 17);  //Element #4 Pin
  EEPROM.updateInt(397, 15);  //Element #5 Pin
  EEPROM.updateInt(399, 13);  //Element #6 Pin
  EEPROM.updateInt(401, 11);  //Element #7 Pin
  EEPROM.updateInt(403, 12);  //Element #8 Pin
  EEPROM.updateInt(405, 3);  //Element #9 Pin
  EEPROM.updateInt(407, 2);  //Element #10 Pin
  EEPROM.updateInt(409, 19);  //Element #11 Pin
  EEPROM.updateInt(411, 18);  //Element #12 Pin
  EEPROM.updateInt(413, 20);  //Element #13 Pin
  EEPROM.updateInt(415, 5);  //Element #14 Pin
  EEPROM.updateInt(417, 99);  //Element #15 Pin
  EEPROM.updateInt(419, 99);  //Element #16 Pin
  EEPROM.updateInt(421, 99);  //Element #17 Pin
  EEPROM.updateInt(423, 99);  //Element #18 Pin
  EEPROM.updateInt(425, 99);  //Element #19 Pin
  EEPROM.updateInt(427, 99);  //Element #20 Pin
}

void loop() {
  // put your main code here, to run repeatedly:

}
