/*  Reef POM 10 Power Bar v8
 *   
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *

Arduino Pinout: 
Pin   Function
--------------
2     Triac #8
3     LED Brightness
4     Triac #4
5     Triac #5
6     Triac #6
7     Triac #2
8     Triac #1
9     Triac #10
10    Triac #9
11    AC Phase Reading
14    Triac #7
15    PCB Temp
18     Triac #3
20    Amp Reading (A6)

10 Power Bar Element List:
Element, Function, Pin#
------------------------
1     , Triac#1   , 8
2     , Triac#2   , 7
3     , Triac#3   , 18
4     , Triac#4   , 4
5     , Triac#5   , 5
6     , Triac#6   , 6
7     , Triac#7   , 14
8     , Triac#8   , 2
9     , Triac#9   , 10
10    , Triac#10  , 9
11    , AmpReading, 20
12    , PCB Temp  , 15
13    , LED Val   , 3
*/

#include <EEPROMex.h>  //For Calibration Data and ID

#include <Ospom.h>  //include the ospom library
#include <EEPROMex.h>  //This must be included for Ospom to work
//#include <CapacitiveSensor.h>
Ospom ospom;  // instantiate ospom, an instance of Ospom Library  Don't put () if it isn't getting a variable

//**If doing Triac Dimming, set phase pin in Ospom.cpp at line 682**  ToDo: make this setable here & internet

void setup() {  // put your setup code here, to run once:
  ospom.Setup();  //ospom.Setup initilizes ospom 
}

void loop() {
  ospom.Run(1000);  //Activates the ospom library every time the loop runs **This is required
        //(333 = Read Sensors 3 times a second, 1000 = 1 time a second, )
    /*
    Put any standard arduino code in here that you want to run over and over again.
    OSPOM Functions:
    ospom.define(Pin Number, Pin Type, PinFunction, PinID);  = Define OSPOM Internet Dashboard accessable Sensor or Actuator Elements
               example: ospom.define(7, 's', ospom.anaRead, "stfw0000");
    ospom.read();  = Read OSPOM Sensors or Actuators and recieve the result
    ospom.write();  = Write OSPOM Sensors or Actuators using this function
       **Writing a pin that is part of the OSPOM dashboard without using this function will cause
           the dashboard to be confused
    
    **Dont forget to Write your own OSPOM Configuation Sketch to setup your arduino, 
       or run the OSPOM Generic Configuration sketch at least once before calling any ospom functions
    */
}

