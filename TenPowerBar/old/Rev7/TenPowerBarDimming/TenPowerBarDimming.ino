/* 10PowerBar Ai Speak Dimming
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *
 *
 
Arduino Pinout: 
2 Triac #8
3 Triac #3
4 Triac #4
5 Triac #5
6 Triac #6
7 Triac #2
8 Triac #1
9 Triac #10
10 Triac #9
11 AC Phase Reading
14 Triac #7
20 Amp Reading (A6)
15 Temp
*/

const int PhasePin = 11;
volatile int ZeroCross = 0;
volatile unsigned long ZeroCrossTime = 0;  //volatile variables are stored in ram, and function faster (for dimming)
volatile unsigned long LastCrossTime = 0;
volatile unsigned long LastTriac1Pulse = 0;
volatile unsigned long LastTriac2Pulse = 0;
volatile unsigned long LastTriac3Pulse = 0;
volatile unsigned long LastTriac4Pulse = 0;
volatile unsigned long LastTriac5Pulse = 0;
volatile unsigned long LastTriac6Pulse = 0;
volatile unsigned long LastTriac7Pulse = 0;
volatile unsigned long LastTriac8Pulse = 0;
volatile unsigned long LastTriac9Pulse = 0;
volatile unsigned long LastTriac10Pulse = 0;
volatile unsigned long TimeNowMicro = 0;
unsigned long LastAnalogRead = 0;

int i = 0;
String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete


int Comand = 0;
int Comand1 = 0;
int actuatorVal = 0;
unsigned long TimeNow = 0;
unsigned long StartTime = 0;
unsigned long LastSend = 0;
boolean SendData = false;
boolean SendQue = false;
int SendDelay = 0;
String ID = "gtp00001";  // <********** SET GROUP ID HERE *************
String IDIn = "";
boolean Pause = false;

const int TriacPin1 = 8;
int Triac1Delay = 0;
int Triac1DelayMicro = 0;
boolean Dim1 = false;
const int TriacPin2 = 7;
int Triac2Delay = 0;
int Triac2DelayMicro = 0;
boolean Dim2 = false;
const int TriacPin3 = 3;
int Triac3Delay = 0;
int Triac3DelayMicro = 0;
boolean Dim3 = false;
const int TriacPin4 = 4;
int Triac4Delay = 0;
int Triac4DelayMicro = 0;
boolean Dim4 = false;
const int TriacPin5 = 5;
int Triac5Delay = 0;
int Triac5DelayMicro = 0;
boolean Dim5 = false;
const int TriacPin6 = 6;
int Triac6Delay = 0;
int Triac6DelayMicro = 0;
boolean Dim6 = false;
const int TriacPin7 = 14;
int Triac7Delay = 0;
int Triac7DelayMicro = 0;
boolean Dim7 = false;
const int TriacPin8 = 2;
int Triac8Delay = 0;
int Triac8DelayMicro = 0;
boolean Dim8 = false;
const int TriacPin9 = 10;
int Triac9Delay = 0;
int Triac9DelayMicro = 0;
boolean Dim9 = false;
const int TriacPin10 = 9;
int Triac10Delay = 0;
int Triac10DelayMicro = 0;
boolean Dim10 = false;

const int AmpPin = A6;
int AmpAvg = 0;
float AmpValue = 0;
const int TempPin = 15;
int TempAvg = 0;
float Tempf = 0;
float Tempc = 0;

void setup() {
  Serial.begin(57600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);

  pinMode(PhasePin, INPUT);
  pinMode(AmpPin, INPUT);
  pinMode(TriacPin1, OUTPUT);
  pinMode(TriacPin2, OUTPUT);
  pinMode(TriacPin3, OUTPUT);
  pinMode(TriacPin4, OUTPUT);
  pinMode(TriacPin5, OUTPUT);
  pinMode(TriacPin6, OUTPUT);
  pinMode(TriacPin7, OUTPUT);
  pinMode(TriacPin8, OUTPUT);
  pinMode(TriacPin9, OUTPUT);
  pinMode(TriacPin10, OUTPUT);
 
}

// the loop routine runs over and over again forever:
void loop() {

//Dimming outlet controll section
  ZeroCross = digitalRead(PhasePin);  //Check for a Zero Cross
  TimeNowMicro = micros();  //Read the time right now
  if (ZeroCross) {
    ZeroCrossTime = TimeNowMicro;
  }
  if ((ZeroCross) && (ZeroCrossTime > LastCrossTime + 7000)) {  //avoids repeat ZeroCross measurements
    LastCrossTime = ZeroCrossTime;                              //There are 8300micro seconds in half a sign wave @ 60Hz
  }
  if ((Dim1) && (TimeNowMicro > LastCrossTime + Triac1DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac1Pulse + 8000 - Triac1DelayMicro)) {
    digitalWrite(TriacPin1, HIGH);   //Turn on the Triac if it's time, and you havent already just done it
    LastTriac1Pulse = TimeNowMicro;
  }
  if ((Dim2) && (TimeNowMicro > LastCrossTime + Triac2DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac2Pulse + 8000 - Triac2DelayMicro)) {
    digitalWrite(TriacPin2, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac2Pulse = TimeNowMicro;
    //Serial.println("Triac 2 On");
  }
  if ((Dim3) && (TimeNowMicro > LastCrossTime + Triac3DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac3Pulse + 8000 - Triac3DelayMicro)) {
    digitalWrite(TriacPin3, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac3Pulse = TimeNowMicro;
  }
  if ((Dim4) && (TimeNowMicro > LastCrossTime + Triac4DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac4Pulse + 8000 - Triac4DelayMicro)) { 
    digitalWrite(TriacPin4, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac4Pulse = TimeNowMicro;
  }
  if ((Dim5) && (TimeNowMicro > LastCrossTime + Triac5DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac5Pulse + 8000 - Triac5DelayMicro)) { 
    digitalWrite(TriacPin5, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac5Pulse = TimeNowMicro;
  }
  if ((Dim6) && (TimeNowMicro > LastCrossTime + Triac6DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac6Pulse + 8000 - Triac6DelayMicro)) { 
    digitalWrite(TriacPin6, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac6Pulse = TimeNowMicro;
  }
  if ((Dim7) && (TimeNowMicro > LastCrossTime + Triac7DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac7Pulse + 8000 - Triac7DelayMicro)) { 
    digitalWrite(TriacPin7, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac7Pulse = TimeNowMicro;
  }
  if ((Dim8) && (TimeNowMicro > LastCrossTime + Triac8DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac8Pulse + 8000 - Triac8DelayMicro)) { 
    digitalWrite(TriacPin8, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac8Pulse = TimeNowMicro;
  }
  if ((Dim9) && (TimeNowMicro > LastCrossTime + Triac9DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac9Pulse + 8000 - Triac9DelayMicro)) { 
    digitalWrite(TriacPin9, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac9Pulse = TimeNowMicro;
  }
  if ((Dim10) && (TimeNowMicro > LastCrossTime + Triac10DelayMicro) && (TimeNowMicro < LastCrossTime + 8000) && (TimeNowMicro > LastTriac10Pulse + 8000 - Triac10DelayMicro)) { 
    digitalWrite(TriacPin10, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac10Pulse = TimeNowMicro;
  }
 
    //Turn off Triacs so they don't stick on at zero cross
  if (TimeNowMicro > LastCrossTime + 7000) {
    digitalWrite(TriacPin1, LOW);
    digitalWrite(TriacPin2, LOW);
    digitalWrite(TriacPin3, LOW);
    digitalWrite(TriacPin4, LOW);
    digitalWrite(TriacPin5, LOW);
    digitalWrite(TriacPin6, LOW);
    digitalWrite(TriacPin7, LOW);
    digitalWrite(TriacPin8, LOW);
    digitalWrite(TriacPin9, LOW);
    digitalWrite(TriacPin10, LOW);
  } 
  
//Measure Amps and case temp once a second, then average them
  TimeNow = millis();  //Check the time
  if (TimeNow > LastAnalogRead + 1000) {  //Read Amps & Temp once a second
        //Read Amps and Temperature, average them
      AmpAvg += analogRead(AmpPin);
      TempAvg += analogRead(TempPin);
      i++;
      LastAnalogRead = millis();
      
      if (i >= 10) {  //Once every ten seconds average the Temp and Amp readings
      i = 0;
      AmpValue = AmpAvg / 10;
      AmpValue = AmpValue - 425;  //Calibrating Amp
      AmpValue = AmpValue / 17.61;  //y=17.61x+425
      Tempf = TempAvg / 10;
      Tempf = Tempf - 178.3788;  //Convert to Farenheight (subtracted 1 Feb4th)
      Tempf = Tempf / 4.4138; //y=4.4138x+179.3788
      Tempc = Tempf - 32;      //Calculate 
      Tempc = Tempc * 5 / 9 ;  //Celcius
      AmpAvg = 0;  //Reset Averages
      TempAvg = 0;
      }
  }


//See if it needs to send a continuous data stream for 60s, and do so
  if ((StartTime + 60000 > TimeNow) && (SendQue)) { //do this for 1 minute 
    if (Pause) {
      SendData = false;
      Pause = false;
    }
    else {
      SendData = true;
    }
  }
  else {  //  Stop Sending Data
    SendData = false;
    SendQue = false;
    StartTime = 0;
    SendDelay = 0;
    LastSend = 0;
  }
  
  if (SendData) {  //Send Data at the specified Rate
    if (TimeNow > LastSend + SendDelay) {
          //Send Back Sensor Data to Ai
      Serial.print(ID);  // Group ID
        Serial.print("/");
        Serial.print("eam00000:");
        Serial.print(AmpValue);
        Serial.print(",");
        Serial.print("etf00003:");
        Serial.print(Tempf);
        Serial.print(",");
        Serial.print("etc00003:");
        Serial.println(Tempc);
      LastSend = millis();
    }
  }  


//Deals with incoming Serial Requests
    if (stringComplete) {
    Comand = inputString.charAt(8) - '0';  //converting char to int
    Comand1 = inputString.charAt(9) - '0';  //converting char 2 to int
    if ((Comand1 >= 0) && (Comand1 <= 9) && (Comand1 != ':')) {
      Comand = 10;
      Comand += inputString.charAt(9) - '0';
    }
    
    switch (Comand) {
      case 0:
        //read device ID from flash memory
        Serial.print(ID);  // Group ID
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;
      
      case 1:
         //Send Back Sensor Data
        Serial.print(ID);  // Group ID
        Serial.print("/");
        Serial.print("Amps,");
        Serial.print(AmpValue);
        Serial.print(",");
        Serial.print("Tempf,");
        Serial.print(Tempf);
        Serial.print(",");
        Serial.print("Tempc,");
        Serial.println(Tempc);
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 2:
        //Set Triac #1 ON, OFF, or Dimmed
        Triac1Delay = calcfunction(inputString);
        if (Triac1Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin4, LOW);
          Triac1DelayMicro = 8000;
          Serial.println("1");
          Dim1 = false;  
        }
        else if (Triac1Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin1, HIGH);
          Serial.println("1"); 
          Triac1DelayMicro = 0;
          Dim1 = true;
        }
        else if ((Triac1Delay < 100) && (Triac1Delay > 0)) {  
          Triac1DelayMicro = 80 * Triac1Delay;  //convert to Delay:
          Triac1DelayMicro = 8000 - Triac1DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim1 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;  

      case 3:
        //Set Triac #2 ON, OFF, or Dimmed
        Triac2Delay = calcfunction(inputString);
        if (Triac2Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin2, LOW);
          Triac2DelayMicro = 8000;
          Serial.println("1");
          Dim2 = false;  
        }
        else if (Triac2Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin2, HIGH);
          Serial.println("1"); 
          Triac2DelayMicro = 0;
          Dim2 = true;
        }
        else if ((Triac2Delay < 100) && (Triac2Delay > 0)) {  
          Triac2DelayMicro = 80 * Triac2Delay;  //convert to Delay:
          Triac2DelayMicro = 8000 - Triac2DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim2 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;  

      case 4:
        //Set Triac #3 ON, OFF, or Dimmed
        Triac3Delay = calcfunction(inputString);
        if (Triac3Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin3, LOW);
          Triac4DelayMicro = 8000;
          Serial.println("1");
          Dim4 = false;  
        }
        else if (Triac3Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin3, HIGH);
          Serial.println("1"); 
          Triac3DelayMicro = 0;
          Dim3 = true;
        }
        else if ((Triac3Delay < 100) && (Triac3Delay > 0)) {  
          Triac3DelayMicro = 80 * Triac3Delay;  //convert to Delay:
          Triac3DelayMicro = 8000 - Triac3DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim3 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;  

      case 5:
        //Set Triac #4 ON, OFF, or Dimmed
        Triac4Delay = calcfunction(inputString);
        if (Triac4Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin4, LOW);
          Triac4DelayMicro = 8000;
          Serial.println("1");
          Dim4 = false;  
        }
        else if (Triac4Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin4, HIGH);
          Serial.println("1"); 
          Triac4DelayMicro = 0;
          Dim4 = true;
        }
        else if ((Triac4Delay < 100) && (Triac4Delay > 0)) {  
          Triac4DelayMicro = 80 * Triac4Delay;  //convert to Delay:
          Triac4DelayMicro = 8000 - Triac4DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim4 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;
        
        case 6:
        //Set Triac #5 ON, OFF, or Dimmed
        Triac5Delay = calcfunction(inputString);
        if (Triac5Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin5, LOW);
          Triac5DelayMicro = 8000;
          Serial.println("1");
          Dim5 = false;  
        }
        else if (Triac5Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin5, HIGH);
          Serial.println("1"); 
          Triac5DelayMicro = 0;
          Dim5 = true;
        }
        else if ((Triac5Delay < 100) && (Triac5Delay > 0)) {  
          Triac5DelayMicro = 80 * Triac5Delay;  //convert to Delay:
          Triac5DelayMicro = 8000 - Triac5DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim5 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;
        
        case 7:
        //Set Triac #6 ON, OFF, or Dimmed
        Triac6Delay = calcfunction(inputString);
        if (Triac6Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin6, LOW);
          Triac6DelayMicro = 8000;
          Serial.println("1");
          Dim6 = false;  
        }
        else if (Triac6Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin6, HIGH);
          Serial.println("1"); 
          Triac6DelayMicro = 0;
          Dim6 = true;
        }
        else if ((Triac6Delay < 100) && (Triac6Delay > 0)) {  
          Triac6DelayMicro = 80 * Triac6Delay;  //convert to Delay:
          Triac6DelayMicro = 8000 - Triac6DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim6 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;
        
        case 8:
        //Set Triac #7 ON, OFF, or Dimmed
        Triac7Delay = calcfunction(inputString);
        if (Triac7Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin7, LOW);
          Triac7DelayMicro = 8000;
          Serial.println("1");
          Dim7 = false;  
        }
        else if (Triac7Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin7, HIGH);
          Serial.println("1"); 
          Triac7DelayMicro = 0;
          Dim7 = true;
        }
        else if ((Triac7Delay < 100) && (Triac7Delay > 0)) {  
          Triac7DelayMicro = 80 * Triac7Delay;  //convert to Delay:
          Triac7DelayMicro = 8000 - Triac7DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim7 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;
        
        case 9:
        //Set Triac #8 ON, OFF, or Dimmed
        Triac8Delay = calcfunction(inputString);
        if (Triac8Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin8, LOW);
          Triac8DelayMicro = 8000;
          Serial.println("1");
          Dim8 = false;  
        }
        else if (Triac8Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin8, HIGH);
          Serial.println("1"); 
          Triac8DelayMicro = 0;
          Dim8 = true;
        }
        else if ((Triac8Delay < 100) && (Triac8Delay > 0)) {  
          Triac8DelayMicro = 80 * Triac8Delay;  //convert to Delay:
          Triac8DelayMicro = 8000 - Triac8DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim8 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;
        
        case 10:
        //Set Triac #9 ON, OFF, or Dimmed
        Triac9Delay = calcfunction(inputString);
        if (Triac9Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin9, LOW);
          Triac9DelayMicro = 8000;
          Serial.println("1");
          Dim9 = false;  
        }
        else if (Triac9Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin9, HIGH);
          Serial.println("1"); 
          Triac9DelayMicro = 0;
          Dim9 = true;
        }
        else if ((Triac9Delay < 100) && (Triac9Delay > 0)) {  
          Triac9DelayMicro = 80 * Triac9Delay;  //convert to Delay:
          Triac9DelayMicro = 8000 - Triac9DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim9 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;
        
      case 11:
          //Send Back Data for 60 Seconds
        SendDelay = calcfunction(inputString);  //Grab the time in milliseconds
        StartTime = millis();                   //to delay before sending again
        SendData = true;
        SendQue = true;
          // clear the string:
        inputString = "";
        stringComplete = false;
        Serial.println("1");
        break;
        
      case 12:
        //Set Triac #10 ON, OFF, or Dimmed
        Triac10Delay = calcfunction(inputString);
        if (Triac10Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin10, LOW);
          Triac10DelayMicro = 8000;
          Serial.println("1");
          Dim10 = false;  
        }
        else if (Triac10Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin10, HIGH);
          Serial.println("1"); 
          Triac10DelayMicro = 0;
          Dim10 = true;
        }
        else if ((Triac10Delay < 100) && (Triac10Delay > 0)) {  
          Triac10DelayMicro = 80 * Triac10Delay;  //convert to Delay:
          Triac10DelayMicro = 8000 - Triac10DelayMicro;  // MilliDelay=8300-(83*%Dim)
          Dim10 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;


      default:
        Serial.println("0");
        // clear the string:
        inputString = "";
        stringComplete = false;  
        break;
      } 
    } 
}


//Deals with incoming Serial Messages
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();   // get the new byte:
    inputString += inChar;    // add it to the inputString:
    if (inChar == '!') {     // if the incoming character is a newline, set a flag
      for (int i = 0; i<= 7; i++) {  //Read the ID
        char IDRead = inputString.charAt(i);
        IDIn += IDRead;
      }
      if (IDIn == ID) {   //If the IDs (IDs) Match
        if (SendData) {
          Pause = true;  //if it's sending continuous data, stop for a bit
        }
        stringComplete = true;    //Tell the main program there is input
        IDIn = "";
      }   //int z = inputString.charAt(0) - '0'; //converting char 2 to int
      else if ((inputString.charAt(0) == '0') && (inputString.length() == 2)) {
        Serial.println(ID);
        IDIn = "";
        stringComplete = false;
        inputString = "";
      }
      else {
        Serial.println("-1");
        IDIn = "";
        stringComplete = false;
        inputString = "";
      }
    }
  }
}

    //Processes incoming string
int calcfunction(String inString) {
  String actuatorValString = "";
  int StringLength = inString.length();
  int x1 = inputString.charAt(9) - '0'; //converting char 2 to int
  
  if ((x1 >= 0) && (x1 <=9)) {  //If it is a 2 digit request
    for (int i = 11; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  else {   //If it is a 1 digit request
    for (int i = 10; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  actuatorVal = actuatorValString.toInt();
  
//  Serial.print("Actuator Val in Subprogram ");
//  Serial.println(actuatorVal);
  return actuatorVal;
}


